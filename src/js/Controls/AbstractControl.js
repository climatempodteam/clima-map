/**
 * Classe comum a todos os controles customizados do Climamap
 * @property    {Climamap}          this.api        Instância de Climamap passada no constructor
 * @property    {Leaflet}           this.api.lib    Shortcut para acessar a lib do Leaflet
 * @property    {Leaflet.Map}       this.api.map    Shortcut para acessar a instância do mapa
 */
class AbstractControl {

    /**
     * Deixa a instância original do Climamap armazenado em this.api
     * @param   {Climamap}  api     Instância de Climamap
     */
    constructor(api) {
        this.api = api;
    }

    /**
     * Adiciona o controle ao mapa
     * @return {Climamap.AbstractControl}
     */
    add() {
        this.api.map.addControl(this.instance);
        this.registerEventListeners();
        return this;
    }

    /**
     * Remove o controle do mapa
     * @return {Climamap.AbstractControl}
     */
    remove() {
        this.api.map.removeControl(this.instance);
        return this;
    }

    /**
     * Registra os event listeners relacionados ao controle
     * @return {Climamap.AbstractControl}
     */
    registerEventListeners() {
        return this;
    }
}