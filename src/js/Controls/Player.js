
/**
 * Gerencia o player de camadas do mapa
 * @property    {Leaflet.PlayButton}                    this.instance           Instância de Leaflet PlayButton
 * @property    {Climamap}                              this.api                Instância de Climamap passada no constructor
 * @property    {Leaflet}                               this.api.lib            Shortcut para acessar a lib do Leaflet
 * @property    {Leaflet.Map}                           this.api.map            Shortcut para acessar a instância do mapa
 */
class Player extends AbstractControl {

    /**
     * Deixa a instância original do Climamap armazenado em this.api
     * @param   {Climamap}  api     Instância de Climamap
     */
    constructor(api) {
        super(api);
        this.instance = new this.api.lib.control.timeDimension({loopButton : true});
    }
}