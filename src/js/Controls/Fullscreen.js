
/**
 * Gerencia o botão de fullscreen
 * @property    {Leaflet.Fullscreen}    this.instance           Instância de Leaflet Fullscreen
 * @property    {Climamap}              this.api                Instância de Climamap passada no constructor
 * @property    {Leaflet}               this.api.lib            Shortcut para acessar a lib do Leaflet
 * @property    {Leaflet.Map}           this.api.map            Shortcut para acessar a instância do mapa
 */
class Fullscreen extends AbstractControl {

    /**
     * Deixa a instância original do Climamap armazenado em this.api
     * @param   {Climamap}  api     Instância de Climamap
     */
    constructor(api) {
        super(api);
        this.instance = new this.api.lib.Control.FullScreen({
            title : 'Visualizar em tela cheia',
            titleCancel : 'Sair do modo tela cheia'
        });
    }

    /**
     * Registra os event listeners relacionados ao full screen
     * @return  {Climamap.Fullscreen}
     */
    registerEventListeners() {
        const that = this;
        let fullScreenIcon = document.querySelector('.fullscreen-icon');
        this.api.map.on('enterFullscreen', function() {
            that.api.lib.DomUtil.addClass(fullScreenIcon, 'exit-fullscreen');
        });

        this.api.map.on('exitFullscreen', function() {
            that.api.lib.DomUtil.removeClass(fullScreenIcon, 'exit-fullscreen');
        });
    }
}