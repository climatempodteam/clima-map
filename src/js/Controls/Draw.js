
/**
 * Gerencia as features de calcular distância entre dois pontos e desenhar raio no mapa.
 * @property    {Leaflet.Draw}  this.instance           Instância de Leaflet Draw Control
 * @property    {function}      this.foundMarkersCb     Callback para quando encontrar marcadores após um desenho
 * @property    {Climamap}      this.api                Instância de Climamap passada no constructor
 * @property    {Leaflet}       this.api.lib            Shortcut para acessar a lib do Leaflet
 * @property    {Leaflet.Map}   this.api.map            Shortcut para acessar a instância do mapa
 */
class Draw extends AbstractControl {

    /**
     * Deixa a instância original do Climamap armazenado em this.api
     * @param   {Climamap}  api     Instância de Climamap
     */
    constructor(api) {
        super(api);
        this.instance = new this.api.lib.Control.Draw({
            draw : {
                rectangle : false,
                marker : false,
                polygon : false
            }
        });
        this.foundMarkersCb = null;
    }

    /**
     * Define um callback para quando encontrar marcadores após um desenho
     * @param   {function}  cb  Função de callback, será invokado com um array
     *                          contendo objetos do tipo {'reference': '', 'instance': obj}
     * @return  {Climamap.Draw}
     */
    setFoundMarkersCb(cb) {
        this.foundMarkersCb = cb;
        return this;
    }

    /**
     * Registra os event listeners relacionados aos desenhos
     * @return  {Climamap.Draw}
     */
    registerEventListeners() {
        const that = this;

        // Se o usuário desenhou um círculo, pesquisa os marcadores dentro dele.
        this.api.map.on('draw:created', function(e) {
            if (that.foundMarkersCb === null || e.layerType !== 'circle') {
                return;
            }

            var circleLatLng = e.layer._latlng,
                circleRadius = e.layer._mRadius;

            let foundMarkers = [];
            that.api.marker.objList.forEach((marker) => {
                if (circleLatLng.distanceTo(marker.instance.getLatLng()) <= circleRadius) {
                    foundMarkers.push(marker);
                }
            });

            that.foundMarkersCb(foundMarkers);
        });

        return this;
    }
}