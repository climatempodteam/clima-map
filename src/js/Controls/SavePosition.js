
/**
 * Gerencia o botão de salvar posição no mapa
 * @property    {Leaflet.SavePosition}  this.instance           Instância de Leaflet Save Position
 * @property    {Climamap}              this.api                Instância de Climamap passada no constructor
 * @property    {Leaflet}               this.api.lib            Shortcut para acessar a lib do Leaflet
 * @property    {Leaflet.Map}           this.api.map            Shortcut para acessar a instância do mapa
 */
class SavePosition extends AbstractControl {

    /**
     * Deixa a instância original do Climamap armazenado em this.api
     * @param   {Climamap}  api     Instância de Climamap
     */
    constructor(api) {
        super(api);
        this.instance = new this.api.lib.Control.SavePosition({
            title : 'Definir esta posição como a inicial',
            savedMessage : 'Salvo!',
            customContainer : document.querySelector('.leaflet-draw-toolbar')
        });
    }

    /**
     * Registra os event listeners relacionados ao componente
     * @return  {Climamap.SavePosition}
     */
    registerEventListeners() {
    }
}