/**
 * Classe comum a todos os componentes membros do Climamap
 * @property    {Object[]}          this.objList    Lista de objetos com a seguinte estrutura {reference: 'Nome da ref', instance: Leaflet.ILayer}
 * @property    {Climamap}          this.api        Instância de Climamap passada no constructor
 * @property    {Leaflet}           this.api.lib    Shortcut para acessar a lib do Leaflet
 * @property    {Leaflet.Map}       this.api.map    Shortcut para acessar a instância do mapa
 */
class AbstractMapPart {

    /**
     * Deixa a instância original do Climamap armazenado em this.api
     * @param   {Climamap}  api     Instância de Climamap
     */
    constructor(api) {
        this.api = api;
        this.objList = [];
    }

    /**
     * Obtém uma lista de image layers que são do mesmo grupo (que tem a mesma referência)
     *
     * @param   {string}    reference   Nome de agrupamento da image layer
     * @return  {Leaflet.ImageOverlay[]}
     */
    getByReference(reference = '') {
        let objList = [];
        this.objList.forEach((obj) => {
            if (obj.reference === reference) {
                objList.push(obj);
            }
        });

        return objList;
    }

    /**
     * Remove um conjunto de image layers (do DOM e da memória) pela referência.
     * Se o parâmetro estiver vazio, remove todos.
     *
     * @param   {string}    reference   Nome do agrupamento da image layer
     * @return  {Climamap.ImageLayer}
     */
    remove(reference = '') {
        let index = this.objList.length;
        while (--index >= 0) {
            if (this.objList[index].reference === reference || reference === '') {
                this.api.map.removeLayer(this.objList[index].instance);
                this.objList.splice(index, 1);
            }
        }

        return this;
    }

    /**
     * Dá um show/hide em um conjunto de image layers
     *
     * @param   {string}    reference   Nome do agrupamento da image layer
     * @param   {string}    state       Um desses valores: 'show' ou 'hide'
     * @return  {Climamap.ImageLayer}
     */
    toggleState(reference, state = 'hide') {
        this.objList.forEach((obj) => {
            if (obj.reference === reference) {
                if (state === 'hide') {
                    this.api.map.removeLayer(obj.instance);
                } else if (state === 'show') {
                    this.api.map.addLayer(obj.instance);
                }
            }
        });

        return this;
    }
}