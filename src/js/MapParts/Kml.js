/** 
 * Gerencia os KMLs no mapa. Este componente é membro do Climamap.
 * @extends     AbstractMapPart
 * @memberof    Climamap
 * @property    {Object[]}                  this.objList    Lista de objetos com a seguinte estrutura:
 *                                                          {reference: 'Nome da ref', instance: Leaflet.geoJSONLayer}
 * @property    {Climamap}                  this.api        Instância de Climamap passada no constructor
 * @property    {Leaflet}                   this.api.lib    Shortcut para acessar a lib do Leaflet
 * @property    {Leaflet.Map}               this.api.map    Shortcut para acessar a instância do mapa
 */
class Kml extends AbstractMapPart {

    /**
     * @param   {Climamap}  api     Instância do Climamap
     */
    constructor(api) {
        super(api);
    }

    /**
     * Adiciona um kml ao mapa
     *
     * @param   {string}    kmlUrl      Endereço de onde está o KML que se quer inserido
     * @param   {string}    reference   Nome de agrupamento do KML
     * @return  {Climamap.Kml}
     * @see     http://leafletjs.com/reference.html#kml
     */
    add(kmlUrl, reference) {
        if (reference.length == 0) {
            reference = new Date().getTime();
        }

        let kml = omnivore.kml(kmlUrl).on('ready', function() {
            kml.eachLayer(function(layer) {
                layer.bindPopup(layer.feature.properties.name);
            });
            kml.setStyle({color: "#000", fillOpacity: 0, weight: 1});
        });
        this.objList.push({
            reference : reference,
            instance : kml
        });

        this.api.map.addLayer(kml);
        return this;
    }
}