/**
 * Gerencia os marcadores no mapa. Este componente é membro do Climamap.
 * @extends     AbstractMapPart
 * @memberof    Climamap
 * @property    {Object[]}          this.objList    Lista de objetos com a seguinte estrutura:
 *                                                  {reference: 'Nome da ref', instance: Leaflet.Marker}
 * @property    {Climamap}          this.api        Instância de Climamap passada no constructor
 * @property    {Leaflet}           this.api.lib    Shortcut para acessar a lib do Leaflet
 * @property    {Leaflet.Map}       this.api.map    Shortcut para acessar a instância do mapa
 */
class Marker extends AbstractMapPart {

    /**
     * @param   {Climamap}  api     Instância do Climamap
     */
    constructor(api) {
        super(api);
        this.markers = [];
    }

    /**
     * Adiciona um marcador ao mapa
     *
     * @param   {number[]}  latLng      Lista simples com latitude/longitude do marcador. P.ex.: [-23.39,-46.57]
     * @param   {string}    reference   Nome do agrupamento do marcador a ser inserido
     * @param   {string}    popupHtml   Conteúdo HTML do popup (opcional)
     * @param   {object}    opts        Lista de opções do marcador (Leaflet Docs)
     * @return  {Climamap.Marker}
     * @see     http://leafletjs.com/reference.html#marker
     */
    add(latLng, reference = '', popupHtml = '', opts = {}, markersGroup) {
        if (reference.length == 0) {
            reference = new Date().getTime();
        }

        let marker = this.api.lib.marker(latLng, opts).bindPopup(popupHtml);


        if ( markersGroup ) {
            this.objList.push({
                reference : reference,
                instance : markersGroup,
                marker : marker
            });
            markersGroup.addLayer(marker)
            this.api.map.addLayer(markersGroup);
        } else {
            this.objList.push({
                reference : reference,
                instance : marker
            });
            this.api.map.addLayer(marker);
        }
        
        return this;
    }
}