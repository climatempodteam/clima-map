/** 
 * Gerencia as image layers do mapa. Componente membro do Climamap.
 * @extends     AbstractMapPart
 * @memberof    Climamap
 * @property    {Object[]}                  this.objList    Lista de objetos com a seguinte estrutura {reference: 'Nome da ref', instance: Leaflet.ImageOverlay}
 * @property    {Climamap}                  this.api        Instância de Climamap passada no constructor
 * @property    {Leaflet}                   this.api.lib    Shortcut para acessar a lib do Leaflet
 * @property    {Leaflet.Map}               this.api.map    Shortcut para acessar a instância do mapa
 */
class ImageLayer extends AbstractMapPart {

    /**
     * @param   {Climamap}  api     Instância do Climamap
     */
    constructor(api) {
        super(api);
    }

    /**
     * Adiciona uma image layer no mapa
     *
     * @param   {string}    reference       Nome de agrupamento da layer a ser inserida
     * @param   {string}    imgUrl          URL da imagem que será utilizada como layer
     * @param   {latLng[]}  latLngBounds    Representa um retângulo no mapa. Um array de
                                            2 posições, onde cada posição contém um array
                                            de latitude/longitude.
                                            P.ex.: [[-23.39,-46.57], [-20.10, -30.10]]
     * @param   {object}    opts            Lista de opções da image layer (Leaflet Docs)
     * @return  {Climamap.ImageLayer}
     * @see     http://leafletjs.com/reference.html#imageoverlay
     */
    add(reference, imgUrl, latLngBounds, opts = {opacity : 0.5}) {
        if (reference.length == 0) {
            reference = new Date().getTime();
        }

        let imgLayer = this.api.lib.imageOverlay(imgUrl, latLngBounds, opts);
        this.objList.push({
            reference : reference,
            instance : imgLayer
        });

        this.api.map.addLayer(imgLayer);
        return this;
    }
}