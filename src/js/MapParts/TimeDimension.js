/**
 * Gerencia as Time Dimensions do mapa. Este componente é membro do Climamap.
 * @extends     AbstractMapPart
 * @memberof    Climamap
 * @property    {Object[]}              this.objList    Lista de objetos com a seguinte estrutura:
 *                                                      {reference: 'Nome da ref', instance: Leaflet.TileLayer.Wms}
 * @property    {Climamap}              this.api        Instância de Climamap passada no constructor
 * @property    {Leaflet}               this.api.lib    Shortcut para acessar a lib do Leaflet
 * @property    {Leaflet.Map}           this.api.map    Shortcut para acessar a instância do mapa
 */
class TimeDimension extends AbstractMapPart {

    /**
     * @param   {Climamap}  api     Instância do Climamap
     */
    constructor(api) {
        super(api);
    }

    /**
     * Adiciona uma Tile Layer ao mapa
     *
     * @param   {string}    url         Url do WMS
     * @param   {string}    reference   Nome de agrupamento do WMS
     * @param   {object}    opts        Objeto de configuração do WMS
     * @param   {object[]}  layerList   Lista de camadas geradas pela Climatempo (json)
     * @return  {Climamap.TimeDimension}
     * @see     http://leafletjs.com/reference.html#tilelayer-wms
     */
    add(url = '', reference = '', opts = {}, layerList = []) {
        if (reference.length == 0) {
            reference = new Date().getTime();
        }

        if (! Object.prototype.hasOwnProperty.call(opts, 'attribution')) {
            opts.attribution = 'Map data &copy; <a href="http://www.climatempo.com.br/">Climatempo</a>, <a href="http://openstreetmap.org">OpenStreetMap</a>.';
        }

        opts.layers = layerList[0].name;
        let wms = this.api.lib.tileLayer.wms(url, opts);
        let tm = new L.TimeDimension();
        let loop = true;
        let autoPlay = true;
        let dateFormat = false;

        if (reference == 'satellite') {
            tm._currentTimeIndex = 1;
            tm._loadingTimeIndex = 1;
        }

        if (typeof opts.autoPlay !== "undefined") {
            autoPlay = opts.autoPlay;
        }

        if (typeof opts.loop !== "undefined") {
            loop = opts.loop;
        }

        if (typeof opts.startLatestRecord !== "undefined" || opts.startLatestRecord) {
            tm._currentTimeIndex = 1;
            tm._loadingTimeIndex = 1;
        }

        if (typeof opts.dateFormat !== "undefined") {
          dateFormat = opts.dateFormat;
        }

      /**
       * Update the list of available times of the attached TimeDimension with the available times obtained by getCapabilities
       * TimeDimension object which will manage this layer. If it is not defined, the map TimeDimension will be attached when adding this layer to the map.
       * @type {{updateTimeDimension: boolean, loop: *, timeDimension: *, playerData: *}}
       */
      let options = {
        updateTimeDimension: true,
        timeDimension: tm,
        playerData: layerList,
        loop: loop,
        controlOptions: {
          autoPlay: autoPlay,
          loopButton: true,
          timeDimension: tm,
          varName: opts.varName || false,
          dateFormat: dateFormat,
          position: "bottomleft",
          ...opts.controlOptions
        }
      };

      /**
       * Number of layers that can be kept hidden on the map for previous times
       */
      if (typeof opts.cacheBackward !== "undefined") {
          options.cacheBackward = opts.cacheBackward;
        }

      /**
       * Number of layers that can be kept hidden on the map for future times
       */
        if (typeof opts.cacheForward !== "undefined") {
          options.cacheForward = opts.cacheForward;
        }

        let timeDim = this.api.lib.timeDimension.layer.wms(wms, options);

        const { controlOptions } = options;

        let controlInstance = new this.api.lib.control.timeDimension(controlOptions);

        if (typeof opts.defaultTimeUTC !== "undefined") {
          controlInstance.setTimeUTC(opts.defaultTimeUTC)
        }

        this.objList.push({
            reference : reference,
            instance : timeDim,
            controlInstance : controlInstance,
            layerList : layerList
        });

        this.api.map.addLayer(timeDim);
        this.api.map.addControl(controlInstance);
        return this;
    }

    /**
     * Remove uma TimeDimension do DOM e da memória
     *
     * @param   {string}    reference   Nome de agrupamento da TimeDimension
     * @return  {Climamap.ImageLayer}
     */
    remove(reference) {
        let index = this.objList.length;
        while (--index >= 0) {
            if (this.objList[index].reference === reference || reference === '') {
                this.api.map.removeLayer(this.objList[index].instance);
                this.api.map.removeControl(this.objList[index].controlInstance);
                this.objList.splice(index, 1);
            }
        }

        return this;
    }

    /**
     * Controla a opacidade/transparência das time dimensions
     *
     * @param   {string}    reference   Nome de agrupamento da TimeDimension
     * @param   {number}    opacity     Valor entre 0 e 1
     * @return  {Climamap.TimeDimension}
     */
    setOpacity(reference, opacity) {
        let layerList = this.getByReference(reference);

        layerList.forEach((obj) => {
            obj.instance.setOpacity(opacity);
        });

        return this;
    }
}
