/** 
 * Gerencia os WMS do mapa. Este componente é membro do Climamap.
 * @extends     AbstractMapPart
 * @memberof    Climamap
 * @property    {Object[]}              this.objList    Lista de objetos com a seguinte estrutura:
 *                                                      {reference: 'Nome da ref', instance: Leaflet.TileLayer.Wms}
 * @property    {Climamap}              this.api        Instância de Climamap passada no constructor
 * @property    {Leaflet}               this.api.lib    Shortcut para acessar a lib do Leaflet
 * @property    {Leaflet.Map}           this.api.map    Shortcut para acessar a instância do mapa
 */
class Wms extends AbstractMapPart {

    /**
     * @param   {Climamap}  api     Instância do Climamap
     */
    constructor(api) {
        super(api);
    }

    /**
     * Adiciona uma Tile Layer ao mapa
     *
     * @param   {string}    url         Url do WMS
     * @param   {string}    reference   Nome de agrupamento do WMS
     * @param   {object}    opts        Objeto de configuração do WMS
     * @return  {Climamap.TileLayer.Wms}
     * @see     http://leafletjs.com/reference.html#tilelayer-wms
     */
    add(url = '', reference = '', opts = {}) {
        if (reference.length == 0) {
            reference = new Date().getTime();
        }

        if (! Object.prototype.hasOwnProperty.call(opts, 'attribution')) {
            opts.attribution = 'Map data &copy; <a href="http://www.climatempo.com.br/">Climatempo</a>, <a href="http://openstreetmap.org">OpenStreetMap</a>.';
        }

        let wms = this.api.lib.tileLayer.wms(url, opts);
        this.objList.push({
            reference : reference,
            instance : wms
        });

        this.api.map.addLayer(wms);
        return this;
    }
}