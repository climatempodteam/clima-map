/** 
 * Gerencia os Tile Layers no mapa. Este componente é membro do Climamap.
 * @extends     AbstractMapPart
 * @memberof    Climamap
 * @property    {Object[]}              this.objList    Lista de objetos com a seguinte estrutura:
 *                                                      {reference: 'Nome da ref', instance: Leaflet.TileLayer}
 * @property    {Climamap}              this.api        Instância de Climamap passada no constructor
 * @property    {Leaflet}               this.api.lib    Shortcut para acessar a lib do Leaflet
 * @property    {Leaflet.Map}           this.api.map    Shortcut para acessar a instância do mapa
 */
class TileLayer extends AbstractMapPart {

    /**
     * @param   {Climamap}  api     Instância do Climamap
     */
    constructor(api) {
        super(api);
    }

    /**
     * Adiciona uma Tile Layer ao mapa
     *
     * @param   {string}    url         Url template da tile layer
     * @param   {string}    reference   Nome de agrupamento da Tile Layer
     * @param   {object}    opts        Objeto de configuração da tile layer
     * @return  {Climamap.TileLayer}
     * @see     http://leafletjs.com/reference.html#tileLayer
     */
    add(url = '', reference = '', opts = {}) {
        if (reference.length == 0) {
            reference = new Date().getTime();
        }

        if (! Object.prototype.hasOwnProperty.call(opts, 'attribution')) {
            opts.attribution = 'Map data &copy; <a href="http://www.climatempo.com.br/">Climatempo</a>, <a href="http://openstreetmap.org">OpenStreetMap</a>.';
        }

        let tileLayer = this.api.lib.tileLayer(url, opts);
        this.objList.push({
            reference : reference,
            instance : tileLayer
        });

        this.api.map.addLayer(tileLayer);
        return this;
    }
}