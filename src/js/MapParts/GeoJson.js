/**
 * Gerencia os marcadores no mapa. Este componente é membro do Climamap.
 * @extends     AbstractMapPart
 * @memberof    Climamap
 * @property    {Object[]}          this.objList    Lista de objetos com a seguinte estrutura:
 *                                                  {reference: 'Nome da ref', instance: Leaflet.Marker}
 * @property    {Climamap}          this.api        Instância de Climamap passada no constructor
 * @property    {Leaflet}           this.api.lib    Shortcut para acessar a lib do Leaflet
 * @property    {Leaflet.Map}       this.api.map    Shortcut para acessar a instância do mapa
 */
class GeoJson extends AbstractMapPart {

    /**
     * @param   {Climamap}  api     Instância do Climamap
     */
    constructor(api) {
        super(api);
        this.geoJSON = [];
    }

    /**
     * Adiciona um elemento no mapa
     *
     * @param   {string}    json
     * @param   {string}    reference   Nome do agrupamento do marcador a ser inserido
     * @param   {string}    popupHtml   Conteúdo HTML do popup (opcional)
     * @param   {object}    opts        Lista de opções do geoJSON (Leaflet Docs)
     * @return  {Climamap.geoJSON}
     * @see     http://leafletjs.com/reference.html#geoJSON
     */
    add(json, reference = '', popupHtml = '', opts = {}) {
        if (reference.length == 0) {
            reference = new Date().getTime();
        }



        let geoJSON = this.api.lib.geoJSON(json, opts).bindPopup(popupHtml);

        this.objList.push({
            reference : reference,
            instance : geoJSON
        });
        this.api.map.addLayer(geoJSON);

        return this;
    }
}