const climamapDefaults = {
    tileLayerUrl : 'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
    imagePath : '/dist/images/',
    latLng : [-23.550, -46.633],
    hasZoom : true,
    hasDraw : true,
    hasFullscreen : true,
    hasSavePosition : true,
    hasPlayer : false,
    timeDimension : true
};

/**
 * Classe principal do componente de mapas da Climatempo usando o Leaflet 0.7
 *
 * @property    {Leaflet}               this.lib        Lib js do Leaflet (vinda do constructor)
 * @property    {Leaflet.Map}           this.map        Instância do mapa carregado
 * @property    {Climamap.Marker}       this.marker     Componente que gerencia os marcadores no mapa
 * @property    {Climamap.ImageLayer}   this.imgLayer   Componente que gerencia as image layers no mapa
 * @property    {Climamap.Kml}          this.kml        Componente que gerencia os KMLs no mapa
 * @property    {Climamap.tileLayer}    this.tileLayer  Componente que gerencia as Tile Layers no mapa
 * @property    {object}                this.opts       Opções do componente (merge das opções do
 *                                                      constructor com os defaults)
 */
class Climamap {

    /**
     * Constrói o componente e ativa o mapa no DOM
     *
     * @param       {string}    domMapId    ID do elemento no DOM em que ficará o mapa
     * @param       {Leaflet}   lib         Lib js do Leaflet
     * @param       {object}    opts        Objeto de configuração (opcional)
     * @return      {Climamap}
     */
    constructor(domMapId, lib, opts = {}) {
        // Configura o componente
        this.opts = Object.assign(climamapDefaults, opts);
        this.lib = lib;
        this.lib.Icon.Default.imagePath = this.opts.imagePath;

        this.map = this.lib.map(domMapId, this.opts);

        this.tileLayer = new TileLayer(this);
        this.marker = new Marker(this);
        this.imgLayer = new ImageLayer(this);
        this.kml = new Kml(this);
        this.wms = new Wms(this);
        this.wmsSpecial = new WmsSpecial(this);
        this.timeDimension = new TimeDimension(this);
        this.geoJSON = new GeoJson(this);

        this.setView()
            .setupDefaultTileLayer()
            .setupMapControls();

    }

    /**
     * Posiciona o mapa em uma determinada latitude/longitude. Se a lat/lng não for fornecida,
     * tenta recuperar posição gravada no localStorage, se não houver posição no localStorage,
     * então pega a lat/lng default que fica em {this.opts}.
     *
     * @param   {number[]}  latLng  Lista simples contendo latitude/longitude. P.ex.: [-23.39,-46.57]
     * @param   {number}    zoom    Zoom
     * @return  {Climamap}
     */
    setView(latLng = [0,0], zoom = 13) {
        // Se latLng for === [0,0]
        const   shouldGoToHomePos = ([0,0]).length === latLng.length && latLng.every(function(elm, idx) {
                    return elm === ([0,0])[idx];
                });

        // Tenta ir para a home position (restaurando a posição gravada em localStorage)
        if (shouldGoToHomePos && ! this.map.restorePosition()) {
            this.map.setView(this.opts.latLng, zoom);
        } else if (! shouldGoToHomePos) {
            this.map.setView(latLng, zoom);
        }

        return this;
    }

    /**
     * Posiciona o mapa de forma que fique visível dois diferentes pontos.
     *
     * @param   {latLng[]}  latLngBounds    Representa um retângulo no mapa. Um array de
     *                                      2 posições, onde cada posição contém um array
     *                                      de latitude/longitude.
     *                                      P.ex.: [[-23.39,-46.57], [-20.10, -30.10]]
     * @return  {Climamap}
     */
    fitBounds(latLngBounds) {
        this.map.fitBounds(latLngBounds);
        return this;
    }

    /**
     * Configura a tile layer default do mapa
     *
     * @param   {string}    url     Url template da tile layer
     * @return  {Climamap}
     * @see     http://leafletjs.com/examples/quick-start.html#setting-up-the-map
     */
    setupDefaultTileLayer(url, opts = {}) {

        this.tileLayer.remove('main');
        this.tileLayer.add(url || climamapDefaults.tileLayerUrl, 'main', opts);

        return this;
    }

    /**
     * Coloca os controles no mapa
     * @return  {Climamap}
     */
    setupMapControls() {
        if (this.opts.hasFullscreen === true) {
            this.fullscreen = new Fullscreen(this);
            this.fullscreen.add();
        }

        // Draw sempre obrigatório, por causa do seu container (to do: deixá-lo opc)
        if (this.opts.hasDraw === true) {
            this.draw = new Draw(this);
            this.draw.add();
        }

        if (this.opts.hasSavePosition === true) {
            this.savePosition = new SavePosition(this);
            this.savePosition.add();
        }

        this.player = new Player(this);

        if (this.opts.hasPlayer === true) {
            this.player.add();
        }

        if (this.opts.scale) {
            this.scale = this.lib.control.scale({
                metric: (typeof this.opts.scale.metric !== "undefined") ? this.opts.scale.metric : true,
                imperial: (typeof this.opts.scale.imperial !== "undefined") ? this.opts.scale.imperial : true,
                position: this.opts.scale.position || 'bottomright'
            });
            this.scale.addTo(this.map);
        }

        if (typeof this.opts.controlSelectMap !== "undefined" && this.opts.controlSelectMap) {
            this.controlSelectMap = this.selectMap(this.opts.controlSelectMap);
            this.controlSelectMap.addTo(this.map);
        }

        return this;
    }

    /**
     * Dispara um evento
     *
     * @param   {String}    eventName   Nome do evento
     * @param   {Object}    dataToSend  Dados a serem enviados no disparo do evento
     * @return  {Climamap}
     */
    fire(eventName = '', dataToSend = {}) {
        let evt = document.createEvent("CustomEvent");
        evt.initCustomEvent(eventName, true, true, dataToSend);
        window.dispatchEvent(evt);
        return this;
    }

    /**
     * cria botão para mudar base do mapa
     * @param opts
     * @returns {*}
     */
    selectMap(opts) {
        let self = this;
        const selectMap = L.Control.extend({
            options: {
                position: opts.position || 'bottomright',
                start: opts.start || 'street'
            },

            onAdd: function (map) {
                const controlDiv = L.DomUtil.create("div", "leaflet-control-easyPrint leaflet-bar leaflet-control");
                const controlA = L.DomUtil.create("a", "fa fa-map", controlDiv);
                controlA.style.setProperty("font-size", "13px");
                controlA.style.setProperty("bottom", "0");
                controlA.style.setProperty("position", "absolute");
                controlA.style.setProperty("width", "100%");
                controlA.style.setProperty("background", "#00000087");
                controlA.style.setProperty("color", "#fff");

                controlDiv.style.setProperty("background-color", "white");
                controlDiv.style.setProperty("background-position", "center");
                controlDiv.style.setProperty("background-size", "contain");
                controlDiv.style.setProperty("width", "80px");
                controlDiv.style.setProperty("height", "30px");
                controlDiv.style.setProperty("transition", "height 2s");
                controlDiv.style.setProperty("box-shadow", "2px 2px 7px #000");
                controlDiv.style.setProperty("border", "2px solid #333333");
                controlDiv.style.setProperty("border-radius", "6px");

                controlA.setAttribute("id", "selectMap");

                const changeMap = function (type, select) {
                    let query = select ? 'satellite' : 'street';
                    if (type == query) {
                        controlA.setAttribute("data-selected", "satellite");
                        controlA.innerHTML = 'Mapa';
                        controlDiv.style.setProperty("background", "url('data:image/jpeg;base64,/9j/4AAQSkZJRgABAQEAAQABAAD/2wBDABALDA4MChAODQ4SERATGCgaGBYWGDEjJR0oOjM9PDkzODdASFxOQERXRTc4UG1RV19iZ2hnPk1xeXBkeFxlZ2P/2wBDARESEhgVGC8aGi9jQjhCY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2P/wAARCAEAAQADASIAAhEBAxEB/8QAGQABAQEBAQEAAAAAAAAAAAAAAAECAwQG/8QAMRAAAgIBAwMEAQIFBAMAAAAAAAECESEDEjFBUWETIjJxkYGxBBQzQqEjQ1JigtHw/8QAGQEAAwEBAQAAAAAAAAAAAAAAAAECAwQF/8QAKBEAAgICAwACAwABBQEAAAAAAAECESExAxJBE1EiMmGhQlJxgZHR/9oADAMBAAIRAxEAPwD7PQknPUm2sulk73izgtGEopKH/k8D+VjWJSTOeD5Iqqs0fVvZ13p4inL6FSly6XZHLZrw+M1JdmT19WHz0w+Wv3TQdfo7qKjws9zR54/xUHymjrHVhP4yX0aR5IPEWS4yWxL5x/U2Y/3V9Gyl6JgjSfKsoKEc5QSi2rWOjOUovDvqd5/CX0YkrhFd2YzirLTZL1NvH4M3py/uV/8AZGpanpyaqzl7JP3Y+hSgvuyHM1u2LGfqR0TmknfPTk5x0YyVxn/gk4SglfHjgXWkNSezq4u/c7Zz1VhdjMdRpdxKbkqdGipLBnKdoyCWURmT9zpjUjnEjAus9Vkx5Y4s24pZojtN3kunlryXUWU1xIysNNdAhJvITSTo7y001jDOLTTpnoTtWctblHQ0tkyRgzLlM0SSTWTKauIoOpFBmF7cmhxfZWKSp0CPgpG6f0OWgWz2ppq0Uw04vdH9UaTTVo1T8ZsUAxOahV9RiEtKEuYo5S/hIv4ya+ztGSkrTNGcuKEtopTa0zyx09aEqU08dTfrSh/U02vK4OksaifhnOWuv7Vf2Qodf1dBKa9OkNWE/jJfRs4+npaqulfgj09TTVw1LXaRXacdq/8AgKT0zrP4P6ObkoqDfCVmfWm6UoVfXoWMtJwSu2/yR3UpYG00iSlpSdtO/wBznKr9vBqULl7IuvJhpp0zRnOwpOPDaNepPbV4Mh10sQrJwRX3ZolXgmn4O16KKFF1wBrImqAim5JLkNVyMrwOvsFhmk41slx0Y27JK3jozL5/RFj7tNx6rg5VcEpI6bU20w21J08dCNt8uxFLrKkDdUzGTZXLFUvwIyromQhVInsyfGXhlMrMr6I0ZQe60VyPP9AXLKot9GVacn7ksGn0SlhnsMcTVdeTZzlL3xrLzizWWEbI6Hm/iH70l0R3U03XD7M4a0HvdJuxSdrBE7o5WerRkpQSvKOMdGUvH2dYaKi7t2TFNExTRqeNr8mHpab4tHSS3Ro5ptOn+S9PJpVljpKHubbaD4Tll9F2LqSuO1csm2pxbeWzOV3SKikkajFt7pc9jM9LTm6aSfg3NqMG2eXdLuynGNU1ZLm0zT3Q/p6m5dnkmpPU4nCP2RSlF2nQVzll58mfT6bRPyXtGLfWI3d0zQF1l/uF2j9HTS1dOOHdvwYlW50g5yapt0NyeJL9V0G3JbyP8ZYWCuUdyrdS7GlqLhxVd6ObVeUBpp5Qm3HDNOoytZRXqJr4qwpKVWs/uaemnmLotWFexOVs1pr3WuGibW5bfyal7ltWFwc3Jl9Ua8d1bLsUsJUl1OcltdM7wxaZWk+VZtxwSjjZPIux5rKouWF2Or049EbpLhFdG9mai7OOlBS0lTOiWyNXfgxpe1zi8U7NNuTtUku5EZqMFezdwuVlk3WaQjOo1RmKXVv9zSkuiEpNu7obXh02Lq2/tkmlFKSSVMx67l/T05S8hx15r3SjBdkU5xa/FWKn6dmk+VZnY18ZV4fBz2a0fjqJ/aFa88Nxiu6G5fcXYV/TUtVQxqY+jPruX9PTlLyahoQjmtz7s6DS5Ht0FxOPrxeJJwl2Yk91pV9s6zjGSqSTXk8voq21bj9kTc1jYLq8m/UrKzN4SRVo6j90tRqXjoZgo6cNySs167p4CMeyuYpTUXSJOOrGNPUi15RxuS6WabcnbZdksYeeAcPpszc78Mxe51w/JXWKvyV6dL3xZI6W+VRv8i/NY2FRf8LGEpvCNamnsrN2Z1NOMXS3fqzO5xi403HlZ4H2a/ZB0i8J5KDO7wyq+wKaeiXFrZqMqxyuxcrCSafciltaaSf2dnqSene3HdMKrKwXHOGc1SluXC5Kp1jpyHj3SSdcUdIwajb5YJTb2afitHO7Ummr/Y0tu2uxJL3SXdGo1KKbXQiEX2kim8Ixeefya3vqjm16Mt1XF/4O0WpJNcMrjUraumKVbM70PUjdXntRNWKlGupNN749pLkbnNS64ClVkfycqpv/AOwaja4j+TUa2p9zQQ4v9TYnLw5zTcW2/wAGtvaTQl8X9FXxT7mnVdhXg6FANCQAAAEKc9SS4fC5/wDQm6AqW/L+PRE1XthZyevK8JJdjE5OeWRaSJcr0ZXBuGnKatUWtOFcya6G/wCYxiP6WEY+EurySGlOMk8YO553/ESl8Y7fLOc92/Oo3XFGvVLDYdktHfU1YuLjFps5ac9jbqzCSTtr8HWWnbi4KkyHnKFbeRPU9SNbcrJzOmyem+aT6o5ksH/SNWROnTOsYRcHJvjoWMFP5L7M5Rd2tlwzh6ObVDc9tW6NamjBK0v8nOMYp5VrtYNzWK/yDjFPY3qsu2d46/tSjpzf6GFNxftSivojnJ8yYJTS3/gfaKeEbU3OduO2kZ/1FG90Yx6XyGpbFFZlLk1HRSzN7n/gxgpTba/+G7aSOSWpqf3Pb3eDem/Sm9OTx0Z2OE0o6kvUVqXD7Fy4/jqSeRKXbB15n9HO2tZSX93Qf6kI0oqq5C03uUpu/CFKTfntjSSNqTi6q/HU0pxeLz2YkvbjlcBOM10Zuri6szdM05KMeM8Eg24bWsxOa+dLhFU3CDdfLh2QuRy5P4htKMTrvXqOBs8tbZ++X4eT0ppq1lG6ZknZQR45FqrvAyiSlSxy+AorbTz3JFW9z68HPUWpC2pNx/Yj+g3RylSbUXaMm4QcpK06O09FNe3DFVmVN5PPFOT2pWzelBSm1Lp0NOHpyddVi2Z9SUG3XPdEppYZbi9mtbTrMeOxxb6O/wAHeE92ZSSa4MaiaabkpWN28oTSOdnfRuUaTcUv8mFptvEk7O+mnGNNJPwCi/Qjsbf+0vyc3p74qUXfezrOW2LZx09baqlx4KaiU5eGJJJ0406N6ck41hMxqT3yvp0M5T7MlUngjs7OmpNVtRzDduwDdkt2CX/al7pYRTMH793bgz5HSr7NONW7O3EYy/48nQxFqW6uLLprekm3SRPDKm4m81eQ5xTSu2w471UuOyOOsn69JXVUj0nXLjwr9MYztujg29NOEuOjNS1U1UYt3wy6L9aL3JYfBH7U/wDrI5eWMuLF4NYNTybg7VNUScN2UlYeJ33LKSismqScesvCW6do58Rn0rB11HF6exZvhGMu3JUmu5lSST2V5bOVSptR9NGrqxqU9slXuOupFqCUFhZE1Frao3XboTRk43GbquLOu1dHP1Zye+bd3g1pStqD+NnaTr41uYThFqF5CrdDSrJsjVqiTlsjufCOP8x73SbTKcktjO6SiqXBHJR5ZKlL5SpdkYjqaaTr2vygthhFm7cZONJPqXVg5pVWDhLVlN30NevLbXXuRFrIpSWiT01BZbsxGk88BycuW39gDJnTSjFu3KqydvUi+Hfg8oTp2NOhqVHTV1d+FwcwBXYm7HJ39K0nJ3SxRwNepLaleBoaa9MvkACJMydRKlUaMvM0uxszj+Um/wDo0eIpF0pU6fU6wb07xuXjk8/Vo3Cbjh5X7GVOL7RNYyTww5SnqqfD6I9COOxxlcfwaepKvg76HQueMtkrjcbMRnHR1pf8WuhrctRyaTSfcxsivdNmoz3RltVVwZ83KuRYRXHFxeTVyaSaS/UjSUt02r6HFakusiOdyb5JnxzVdvQXJF68LqTcpUnhBRxkifZGjfr1iotGDlbs9iSiqI5Q4bTPPGW15SaNetJcJV9DtmndGpwS05SzF9PBwyblqSmqbwZJpeESlbK5Nr3O/sgH6hRLbeza1G04yymc5cOixe12WepuSSSVdgesjWSKLcd3QEWEUFoTAAGIAAAAAAAAAAEeAZ+T8ESlWFsuMby9Fh1fc0Qo4x6qhSduwQoKJCtcNou6X/L/AAQEfHH6L7y+zE3+rZvS9sku5KV2OMroVOvj6RCDffsxJVuXYwlTV9TrqpOn0aMNOeIqzL5F8NemnR/IVdjLk+gluTpqmajH22unUuMk0Zyi7KACyAAAAAAAI3jgcB8FJ9K8AAKJAAAAAAAAGZSSE2oq2NJt0jQMJSfWi7O7ZHdvSL6pbZJZpdzXBFFI0OKdtsUmqSQABZAAAAAAAAAlgBuMd+nXZhyUfbDnuc7leMLqG6VI51xZzo6Hy4xs29TFTimY+K9t11QX7HTS1NktrSplLiV2hLkbwzIANjAjwAUAABOAGH2KRdykr7G/oAAokEtFAAACN1yLQySdIzGN5ZUtzt8GjFR+SXZ68NW+i6rZQAbmIAAAACN0gApAsImX4QAXcjO9v4qy7cmsJYQDMKM27bNOoKw2zMsqmACLcss15EVSOkoQSWb75pCf8HFWXTWk0k7v9znKD4rJZasVwr+sIwtaSlaSS7GseKbX0DlH00CFMySMoAARuhXVhdykrOStYIUhSiQAAAAEACSdIkVeZD5TrojRil3k29I2b6RpbYKQpsYgAgAUjb6IES6gA45GW8FrBQAhQAAjvoUFcGuQGk2QzK2126leoo4jl9zm228s1hxSl/BNpGnNfZlycuWQHTDjjDRm5NgAGhJ1RQDzEbAjKRqwehrYKskXNFToEDK8UjNWUDAAEAA3SyZuT6L9Qludv9DZkrnm6RbqONszGNW2aALjFRVImTcnbAAKJABHnkAHJQAAADkABrZKrx+SvatOqV9WzjLUdUn+pUYOeimlH9jbko/ZzlNyeTIOqHFGOdsyc28IAA2IAAAAAAA7AEPNNigllTp2JOx1RFyyl3XB+1c8kBaHLYAAyQQjkl9k90vCM3NaWS1B7eCw4NGarhlz4CFxikwlTdooJfgOuLL7IVMN0UiVFGSAQWu4rGUEtG1H2Nze1AneENRZmK3OupZSjDHL7I5b2vjgydMOD2ZL5K/U1KcpPL/QyAdKVaMm7AAGIAAAAAAAAAA615YryUHl9UdFslDK+g3XJnMvCJk0sLZUVe9G37Uk2u5N6qrRNq7FSiuiBd/4D6X6Zc+2S03y6+i4aC4F1bf5MOyS/FBJLhAoNEksIhtvYIgUYgQ3jbwYAYpDbjllVPkjkuhPVDtikKI5dgm+w6QrZ2ShHT3SOGpqObzhdEJvCRg7eKKqyJy8AANjMAAAAAAAAAAAAAAAADsZcq+w5XiP5CjX2eS5OWI/+nV1UcyEY3JObNOrxwAVGKjomUmyNWKRSFCJ9YKlQXBSUvRt+GcvgZXktFKJEY9ZFk1RDLvoAzM9RtUkzKhJ9TquCgBxqavJtRwaKAjKVGiGZT7FRg5aBujMlUmQA70qVGQAAxAAAAAAAAAAAAAAAAAdtrhhqgdXLSlbaaYhHSccvPk81RrCOhq2cgdJ6LjmOUcwqhNUCPhlI+BS0C2CkKMAAAEAAAABV1AACNpElKvs5t2bcfF2y9EuVFbbIAdaSSpEAADEAAAAAAAAAAAAAAAAAAAAf//Z')");
                        self.setupDefaultTileLayer(opts.layerSatellite, {maxNativeZoom:17});
                    } else {
                        controlA.setAttribute("data-selected", "street");
                        controlA.innerHTML = ' Satélite';
                        controlDiv.style.setProperty("background", "url('data:image/jpeg;base64,/9j/4AAQSkZJRgABAQEASABIAAD/2wBDABALDA4MChAODQ4SERATGCgaGBYWGDEjJR0oOjM9PDkzODdASFxOQERXRTc4UG1RV19iZ2hnPk1xeXBkeFxlZ2P/2wBDARESEhgVGC8aGi9jQjhCY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2P/wAARCAEAAQADASIAAhEBAxEB/8QAGQAAAwEBAQAAAAAAAAAAAAAAAgMEAQAF/8QAMxAAAgEDAgQGAQQCAwEBAQEAAQIRAAMhEjEEQVFhEyJxgZGh8LHB0eEyQhQj8QVSFST/xAAXAQEBAQEAAAAAAAAAAAAAAAAAAQID/8QAHhEBAQEBAQEBAQEBAQAAAAAAAAERITFBcWFRAhL/2gAMAwEAAhEDEQA/APDdxaIYGVwQOhB2+s1WgVlBEmQDO4Jj8FQsq3yfEkHku3LPuassHw0GlSAoxOx/I+q52Oc9adKDJICmCCd5/uTW22XUCpBJ/wAeuw/iguL4yliRIMkxmkcOWhQxZlIzmMTO3oaKc7AsVK4AB54zn9d6QFYylq4Rp37bZ7f1TWtBBqLAYgmMDOxjf+qK28AsMQcRttjFTyHtBxJcWRbCEFF0kjn3ouCW21mF8xA0s3IzP8xTRbMKQSpnJJydzkj5qW+wsXw7CLTSxgSARvj1NN08VBiVXVgkTJO9BqnVEAYMjlO4PzNbhrYIBbGJJI9RP2Nu1LDabJhFJUyABI2Hz/7SKxCxukrux/cwKKQbiliDpy3XvQ8OdUxIAyMROJFc2h3JOVwSBg9wfqqzArd13SUIKnodhuB805QYYs0qeRMQJiuCeGv/AFoCWEERketaFS0oAlQBggkTz609Ah4QLAJJ3AJ7Um4SjqGYlR0BMcoPamt4LoyjzEGRJMfPuaI6TbbVhhhgJjfp80WwDKJUjSUbO8dqNJ1q7yARBUZjnNaysoIQgEiSSMmD1pP/ACVUEDTPIgRv+GnqeD8Hw2DgalA/xIkznelu+l4uMpzgZnnRHiVKRm4TyBI+6muFmZWWwQRgRiPcVZDxWLoyyqxAwR032/ip71wqoDbEwDBEb8+n811y1ce2ofSigSRAJ9a0WZAZ2a7sIJOMdPSnIbQq5LJohhGeWP52pq8Pe4m4EvOFB5xvvgn6rAwAEkoudhkGaOwdJLByWmR5px1j96nhP6bcsJw4izpIBjUYz8UtmAhnc6RmdusfdE5JCy5aDzJ39vU/FOYK1pVCyBgxMHb+KLekeOqWoILqQQCOfU0l7zlcoQoncRI6UTMyW1S0ADBlTyyM/dTJbZr4UcjAIMAew2qzrPdNtM1yS0ySFAAONsn4qok6GAkkcxsMde1bChGBMSJJnP3QO9y45tEBgF3GJipVcWJfSuokAGIwfwz9UahNImdwRHOkXGuKotqSrCDqnG9HatPANxtUAgiTnODvvSrK4AWQoKBmG0NJ+PWqUfxLTQInEYBG5wPQb+1TJ/2SwJMbR096baJtiTmNvz3qI1gVBIEwIGcEfn1QC2Who0qF5nryo2ZQupsFmk9uUj4BoVdRcKknzANjl2+T9UUt9SHSSCrbT0yaK06rK7A4JO8x/XpRGybr4YE8p5/mI9KU8oxJWQDB6Dv/AO09TwbZuswZgoBgasbes7YrrgS8GUyQSCfWfzf+6EOFILEKOo58v5+K3UCCqEaRk+nKmLsrS2m2oLEMBgexxM/t/YsYQBY3wQ0kT3/P2rXUNIMADfqKWjIbzQZ6z0mJikiUwjRaAWTIk9Rjl+cqVaYkEKM9Ykf3sK179u4xVPMecbARXaWSDbt4Kmcyexn82q/oclxldSJMglsbGOvvWGLq5MTOIgnsfzlS/DkjUYBzApbWyLoh9Kjodx1+6YSqFFtVNsCI/wAhzOZj5xXArqYltM/A7H2rlKB9snfaT+T90SiH1OAVO/Qd6YsDd4mzq02lZiMGYknGfTFYthWfz29TEDn+kCjaUuaVXSxIhtuYzRO4DBZBJMEzPz9VPw/WrKgstqOzQB6/maBka6C2qEA2B2E7E9dqHiXKCQwWN8xO1LPGXCdKCAwE8vafir02Nbh20jQrvI8x5Hnj85ViJdlpwgGZIweX1+tAr32wqDIkydxQqLwkqQgMkg5jlTqaMu+ogKGVhGCDFEqm1DOFkDcYyQf5oNSazquk8429q0m5rIDNpIgSMj1+6Blp9QlpC7xsQIG/LemF20TbJByDAwB2+fepv+T4SmDJI5yR09qdZvHQS0tsYG4zkfNMCQb7kqCkECTzEzAHxTrFlOHJkyzQRkfqP1o7j+GjPGktuZnvv3k1Mt033DFYGIO084709OQ1wwuEnKsYIO/zmiuFlIBSIGD1nl9D83S73GY+aCp2EHYUdsXGdSxmNw0HHU0KIWzcua7hnVgcupjv9fpWh1tsyA6jOTGAMc/b6rZD62toSFyYEDtWoFYKIJJ39YyBT1S10hFRVIacEZA3k+lMRtIKkltXpIjf89Kme5qFvQrAA/5RMdST81zPdJJmAOgySOfamVFF1WmVIJ6RPqe/xQrqYC2NOrUdUbEf1S3e74isqEqRTLasGLFCAZmeh3NQLvXQrm2rSQJAkj9unehZ3u6gCFJwZ3OfXemNbN5wVEKDmdz+YpzWVaNJkiAeee/TJq6nUrcLKeZiXG5+9z0oxw6eHoDEkbEmTtzFUQNIkaxMDECiUhCCoBbaRz/BTaqMC+V0gqD1IJkfh50Y4dQuAWaIljt/WKra8biANACiAeZpYbmRAmSTSVcTrw+iBhRBwNj6kzTwu5DQBzIyawwGLloQASehEZri4KBwZnnvINS7ScC1uQWUkEZxv90NvQpJYM8CdJ3Jpktq0qZDCMzgCuNpUcKv+QkZOeePzlVg1VViZtspiTJB7DasQG4SXE6SRAMDnBjntRWbrYa4FAIIWczvBpd8hTpDQGnrtULDDeLMAyFcnMxjfqe/zQFQGMASDBG5md/eli9bcAlwAThW6e9CL1tjAuqpJAE8+33V6mw8KsZtZgmTAjr+GtGmQAhGYiNveaQrPrAZwARMzMkbj3pjvC6A4LAf7QOQiocG7eQBbYUAbmD77/VA9w+YhRB307/pRagV82O0UpbgKsGUjMe34aLpdzwwDcMkH/I8/j851y3lYL4cyogbgTPX7pPFXV/wtyQoGAP1+aHh7d1yoKwgOScH8iqlpq2bl+5Jgz/rOB3/AD2qpibACJk7EgD66j3rVQIUW2w8p80HJ23+/mtdC0AMQ2GHcfwaaJOI4e40wSx3ycb5pdq5cRCtlTk74IHWrx5bUuCSu+O39VKXtS/lKAcxuT1pP8LPptjhy2l7rFiTOgYJzmT2FNZld2RDpXYjc8+f5tS1JnSpknIb5j9q21b0I0mWaYJ5mgs4a5w9lLgdCUUeUSc5PKY51FxDM7k2joY5WSTz2xj2ipbl02mYswNyRIGAOUff6U9brsqlVkggkgcsfhp4tuzDU0lWVjkdDMY2271uoKkpg8yeWdvquWw63otr/kJBAk4x+lDc4dC6sGjBO8g5/O3eomDt3FUlSwLk4UmYHX6o0viJDA6jBzvyxUdxnN9T4SnTIkNIaevvTF/63grIYTI5Z29auG09uIU2/KxbOYPOoXe4hJKoFcgxkmN/TnVFlEYSqMrA7EYOd4zimpw7atagFRA6gb8/ek4dTi42hTqJIIJ99t801JJ1EgA5IOcRGKI2iFZtMZIJ3gR/dAQioGeMmJOAKALt0K5IgACT0b0/ul/8xHAmQoy0iSfzNMucLbuIo5f/AKB3pqWbaP5EBkGRGIiSKaZT+F4deL4UaGJU4A5RMbRvgUvirCyti2QWQDWVgADkB1xVvAsDxDIkWrZGy7AxvUPDXVs3GWFYAkHVORmJ9P2qfjVnwpXCadQlgIB3IncTS7qX3gqwVRkSSYPTG9UOEcwYtvJM5z+EgVgtMZBg6hETEd/05VdTEl63fclWfUAI6ft1ptrgLjHXxDlgNxMx+TVOp2GFAC4EkbD3k7fk0XC3baXyWbWpWfLsMbQd8ZjvTUyJRw/DDyhsEGOZojYs6QFPlA3xI7z+bUd/hVN7FwMsSSuB1/M0IsqDIJONicetC8JuI2osrSAAPMBG2IrEURqvEBgJ8uCMD9oxVNwarZZjpUA+o3k1Fw+i4QSwOkHE5HQ0nTh4MrhnMnmd/b2rBw5Kt4hJWTAmepptgwGYrqBMAjb1mnOoe0sAmDkiBuP2FNxZEVu8quWW15AQCemNp+Kfea2cnUQ2BpMHfP7YrQF8M6dMNJzz6fNJ4a5q8O3MspMRGMEmTRPFNu3pDiCxUiMznp9UNhi9whpVVwck+v5tSrhu2yqW3USQWg5G8+v65GKJ2Wwpd5icwDmNgahohcDlnUEpJ2GCM0Nwal8yASDtkT6R060h+Ka1Y8tkBQSMHBz/AD/FZw125dZnghRtHLaT7/vVz6GcKjs+puWwPKnXmKqSG58xMkn7PeiF0LpClSSBPKJrWyhIgg7T60+6eeJE4dbpuEqQrnUCe3rnPOqEQA6EkKoAOTB39+QFMgqBmYGMduv3XKJck41YEiNv0/O8NqzDbd0WwXuLqYbEHYHB+qlulrp1aDBwBnfr88u9EbyIWbzORHlyB1GaYt5FIbRqG0xJHfP91C3U7akuIqjSCQYNGYQud2fBjaa0u/ieZIQAEDf2+fuicSpKkMRAzjMVUpdu4F3lVHKM4Ix8z8U9b73LYQABCczzzIpIkQHIaCd8epx+/etAgGF0n1kmPqoSmAxyknaMRWXNHhyyx1NKLRhcGfNqkmJz+9MnUhDZBoA1qrm0ZHME7DO3qa1SCSADAbr2A/WhWyqnXknlnHXY0QMlpMHbGKtPrFxdDBiABB6GRVb3bZus4tka0IEYAJO+OxMVIAzXF0bEiAREbR+tGrKyGCwAYA46c9+/OoriQuSAWHQxy/8AKEOxOQJBnGxxWArcddR1TO4j79RXf5ASYAmec+9MGcRdi0VMnVMTsQfT0pFgsrBnJLEkmTiIxVDWhobWZ1AgA4Gx71yKWtAEkEiDjbpVmFjBp4i2fKAZwVxHeuCXlHlcMvIZk5GK7QUP/UgVDgjmfSmofMYEGNuXrPtTxCli9/1uhVgMgneedKXhCq+WQ2qZmYFUXHR2BUDUBGDBGMCY7RR8OHuWVZzuMnEjO4/Sm2ANDKJ0QRHoR1rmuMIAnUgkLyIzv6Yimuv+ivCmNxkYqO+XtuuWZeqjIAJAnPX9Kk6XgQyhhqJXS052OMAUacNdtsbikw8mRzzt6xNMdQ8lxmMnAiOfakcPxF1joDErEBskRtkdZBq4h5VEhiBJEg9sAj6pbgXSFElSYAJx6VrI1twruAuYJwR0FENN2DgRvsYH4Kl4s6la3c4gmYFqcx1p5C2bUARAyFHLr9VlsMHuMTFsZBEdTiO/XvQcSwB1aHMmTobn0+qvp8N4S+l3YywAgE7/AJFOK6Z804gA9a82xw13xAyMUgiAMnfOPevVt29RTxTIG+MjOcUpCwde+ynOcRvXC6tolWJAI6cqM6YOlCRG3MiMGkhdd5SHBwZEE9gKgmCteAdCAQNJB5CIqskrblzEEHMAHqP0pa+UlRhMCeY9/wCZrGllAWPLuY264q7pOCBF1NasVIO0HeZzWI3iyMBgc9ztQQ8EL5pOdgCfTlTkCC4dRwZHcmlGXGbBABnG0dN/k1pJL6lJUSNQOQM5gUYUi40sSrYE/wCvp9Ut2YOJiZIMCQc1AxwIkgiMTzHehXBJBkTE8o61pUBCSRKmfXvSwqjYnJ5HFJ0EGkEkwGYb7A7URyxnc0hioBRc6RIBzPP9aaj3HtAuoBI+M1cGtAJJJOBEct64Akgc9yRtFaCCrHduWPj2rkITExuCIjaou9KcMrKFXUCQSeYMj9jTWZbQDAg9t+kGKC6hdGAwTgTtGR+9NsW9cC40MoCZEgkEGc7ZofQX3L2gxQkBZC8zJ/umWlcpOkAaRudzzH6VLxovG7p4fyxOdsdPqqUujSlptWsDeBIPUdqErtQDsGMNuR25UrD5BIDc4gj/ANpwhsT/AI4g8p3xWECRpnJ5ZB/8oFFQGUEk4knqQIFMCaVItkKNjMmOh+Me1YPKYMZOJxTFKliTD5g+sCP0oQKk3NbFgY3z2rmIRfMIb6M/U1i23vAgSAACdhzjHz9VT/8Az/ERg9wlV1Tmc8gR/FXwm1GdOuXIIMATgeneitrbRSUdVBzJ58se4pbLoIKhm0MN85xMz6GpuJvjh7YshQ05IORnMGiW5VJtWnKoSXOxaZyNz+daVb4JkGl7wSSYAMyc9O4I96l4deI16lZljYyczVzW3AAuXNZLSRg6cZz90uz6nvwS2mVctqBXkZAGM/VKul1cqvmY5mMbdPatVLiqfDuaPNq6nvRsvjEB7pJA5CAfU0+r8SqXVmbWQTkkYj+NqaeIuJa80lgQJjJxvRMGUkajvzJx79PSsc3HWPKs4kYmnpgrV6bekamYZOP3pgUND6CHYdcTk5qAzYJZUIggHVBkdpqrh+KDHyqwmTJAPI0sJVAMhQQCZIkSBy3phhbaKFUETkYJ33mkPbkCScGRnBO0fxTDqZUYZ1QB0E5IqLKXclAhIAk53zWE21uIQwJ232JAg/IpxAiDlhJz6ztSwmociRzAnPLHpipoE3Q0psRmY5HY/U/NMRTcIgywjAxt+9Kuf4kBsDbO8bY+q3hyyAEYbJAB2Owqn0TsQGMgGdiMH8zQ3GUZB9oiKxjFyR5tI+a51a4QVIgnY8+1WRK21b8RlIABKnJxy/muUSxSQQOxyJzTUXTgjSB3xMViqoJYwCd+3UfnemjiSEY9AT3Edvc0hH8TLsCF5jkI/Pinq6kiQIBMevL9aAWluWNd4BjmSOk4/akGqSLgGShbJiQMRG/1RnUq61GpjkDlQqgOFwq4Jnp+fdaSxugqCREHOCfT2/SorL10IJMKS052ETj5qQcUrX1ZgdZGlpMBT0n05VbdthwoYCBupG4zSLvB2/DKCQFEwTnJ6/vV4l0aDU7FTBIyeZ3MjPQTRKraioxBz/HeaXYDcPw58pENJzuBzPtW2DczdJEHIBH5B71ALQt4FgROAok5n3+qepCkrBBMSYI++tAeIS9eZYnQJ1daBAbnE+MGLA7LyA6+1MXf8NV2UgTAY5g7dvqiv8Y9u0VtggAg4znmT1rFYlmYmVGAOQMGakvtbe2pe4ABlgNzE/WaTpvDbrM5W/bHmck3BBgg7D3n+al4bhv+RcdrgJAwBEk+ntT7N4XfJw9sADOdjkT9VSWFpA0CCCcGJ/ireM+lsdAwIULsccsVFdum4pA8kGBnERvVl+2L2lpLAZj++lTXOGa43lA0AYJpP6thVp2dvDZgyyCRMfdXT5FghI2kEkjbYfzUq8IbasUDAwQTsOUfvSGdrVyGfUMmJ5wKZqS2evSbzN5BOkxMT1/N6RxN1LYCmDOcDFSW+LuW404QcoxP70F1ifMxJnJIpnV3jfEy3iEtOwPLNU8HxCC2VOkspkSYxG1RMC7bGq7dkJYd3WIA2O5q4zPV0iVDgkE5IyBkxPpTWIAxBUDC8zt+kbUKOjKoTJByNoxiaAk6m0GSoIg9PyKxjfjCtx+J1lm0qsepkZHsOfSttAjXIKqACoGeskRvkTRNelgqAkjB9qHxhbJN0gcx91ejiqeJkzq2kYEe9Bcu6cawDmAASRyG/wCYpqsbg1LBnBjYA4xSzw4S5Kkyc6iQR6etJ0vAqjaxJYA5OMien91iiGwQwAiTkiNhRjUYAhie42696xVhVIIIAEkZgxvVQ5Cww0E9hy/BWOQUJB0kyTEA0SiWKjBIxynehldWgKWKjJqKFCrJvqgkbbx0965LyCbdtJiQZ2PpWowtKAxgwdhMQf6rfIqhiZDc5zBohJ1oF0CNWDJj1MevTOKoQ7KSgYATiBzpNx1S5qYO8bACY2zSrfGBpZ7JBJgHcCfWqS56tu2riWQWk6oAOxOQKUykgA5ZljOYAn+KZcuuyglgyDInE5GJ96Q1lWm8ZUjYHbcmcdqilNf8SLRBB5xiB0j5+aeqeUgkqQMzzkRv6D85RWme27zbkRJbmRXpICbCkiQQIHaKt4nrx3NxLzi2SynGOVW8EQLUMPOZnkRn0pF+0lti6mQMtB5UxeLsqpKiCJgyQYxFW+JPVyaQhQruOvwPqoLtvWVRVwDMDp+dMV1k3rzFgwC5EjrP7A1WFFtUGCY/yBmc7VPFvXcPaRQXUFdXLp+Ypd28qMS2bbGNMEknHfbNBxXFgFUTy58wiD6Ui7xPmBCiZIIOYGIz7U7bpbMyDa81vWgDEHMjb0j2/Oc1y4VIgsY5YgD8/amX+Id0OiVUDC1OLh0FDkbzGavE617lxkjxGIBiJIHxtS2mTzgb0YyCoJg5qjh+FZlbWpwAR3HaryJ2us2Rc4W4WQhhsSIA9PmpjbKlgTOkkGBivUtpfUmy0G3ETsQCf2rRYXxAVKkkEETkmoZUtjh1vWDDzH+JAiD0+6qsWNPC6W/yaTDZgxiiNk2rhZRpXkoAjMZpgUl0LEiBsQMUtakYzRfUXCQnUb4xj/yk8QWF8G2CQCREYHePim+Y6ATqkyZG3OAfWsVA7QpUlSDpkz7kc+nb5rKprYulTc1gAeZdyBB9cHP3Qa2Z1uX5ZSeWInbFUoVlgAYYx2HTPpR+GFUC7AScxuMyDkcsfcVdMEAICEEDk2ZA+e1Ku3B4ZZScDHOesD3IpFzxS3h2ywk7tggdp9T6Zp6WWUAB5XoeQ6fW9PPS3fHWl1kkjLbRiB3+RVlk27d5DcPlmBjY/rH12qSyQCy6gYOkGDyyfztTVfVqDCNMHPTrPrSkuDuanvOyZmTGTPffv1/ipWULe1aijExgnMgxVLvpddMEtOx7TSLgDPLDSpaQwJweWD3oXo1aANQhhvOAP2+qZEbYDchiMHoc5pSuVIVhqgkTEge9ERCjPI89hmcVBLdAY6lbIMwVBGI/j69ZW9xnKgIGxnP+XcfNNS2GmXAUf47ywnbof7qgREooMGM8s1dT1lu7rtkC2yaTABM/rSfFa5fZHBYEga8Az6xtHLaqh5CzM6wRgSMHrNKv2y6nTKkCcYMHf3odK4i2LiBVMMThRAnJ/amG81m3bAaVUieeDUSveNyBBzvvMbdP22pN25dZyrHYkwOtM+IdfvvfuaUY6STuACO2KU1rykLJAOaEEqAQM7T0pyO2tdCSQOsietXzw9WcPbXhrYcXCuoyBsIAz9VHc4xnckAJk4/SuvXmYFZGZkRgE771OyQAcZ+qSfaX+GqfGcQSSBuTuK68VBgNqIPrSkZg4gZPfeiuWtMznmRT6esRyxiJxWvcZWggjsabwtt2ujAM/rR37D3LjSM8hud+fv8ArROpA7AyrR3mvX/+dddxpuOWU9ckRzB/moOG4djci5AUGG5kYOfrnV1gI8KhKizgqeZ9ZyMU/wCszFkVcQ66GOrkYG0xSUuJcspdbAGT0GNv0oGteIt1S5MsHxuBz9txSwRw4RGOHOkjfE7xnORUWqUKsWUEtAiSd/emOIIVAQpzv68qwQLLuiKiKORAI9Z39utHw/D3OKtkeZJUkMcekTkY96LOpbrAXILYmBsBvtPes1NqaZURkxgdT8898U57KOC11oYRGSR67/t1qa+9vUCJIESBiZ3nr6VC8YL7MwKKYBEnYHfNaGa6gZt4OcbTn+vaiF1XtaVAE4zgRP7R90PDgs7IC2kGDOcTjHTBqppnDy4bxJPY9a5bgCsWGmPLB78+1NuaVts2CCJg4IGM1Oj6lYtnAAgSR0P4KnpmcGAGIhdKg4gTPpXJqtsQGnMxjA6R80d1DacIcACW3MA74O+9Lfy61MTMzkmOVXTwUKVykwIBOY/qaBy/hMcagc6Rjbcn1zS7l0W1BMshEGCBJ6bUTaVEqJJEmDHbtU6qfUQpNwhZhZkmevbnRNfAUAiQo9M1LdYsxEwDGOtaqWwBN2GMzvitYzqlLha2QQNewEiKPhrhLKiS5iY2jqKmW8oAULn/APW3PnWWOIa3fJH+0EnY/NTDXqWrJy7CbhnJ2O9bclLShYMDLnmef52oWv3HZJUkb+U7A43x1rrys50EhVUySMkzuKnfrX4luO8hVAmMxkA/+1ILLkMWMEZzuRXpX7luzq0aCcETkR+RUABu8QFV8NA6D0+qrOdCA3KSOVUKrKqhhGJgT0/qqeHs27KN4rKLgmUG4MY5+lIuOXJY5IwY2+KbrWYS6qDsRPPpSXzEHqKK+xYDThY3ila3VCsbZyM1ZtZth1pHtqCFBcmV54FA95rjEhYYkyIxW8Pde1dViC3KSSInNVvw+tVv2mAAzgbd808vTbeJuG8R5ZW0hecRPavUSzrt5abhgahkxyJqa1aFxgQq6JziA3f8/wDLtWlhIieXUVKsmegtqiAwBnM9cZJ+AZrLgRUB06yTgTiR17zFGwACgZBAjG4oXURjMAx1HaooWcEh9IDEeYzHrS7q2yAwH+BPmPIRy+qeyi4DaACl8S3LufYVff4LhrnD+CwIVQCShgkwYzt1n+hV3CTZeobdsMCTEIpnvjn80u4XVV0Ektv2xvR3ry20gyFBOOpiDQJdW4GOkhB/tOD2ofxKOIM6nUggYyAYxy/euV2ZzoSVIO4gT0B+aUreJcEYYEgHmTO/xVXDoxbQAUJYwNwfSl4na4Brdosyk52OOe8dKdYKqI2iST0Hry3/ADNHqCaixkgFTGTMnFec965xV2bcBUaAAIB6yPanq3i+9dt3fKikKN4O3KhULaJkAESYHPlNJtWWVNTbsRqJMExgnFHdTSrOWmMDOB3+qidDcuaVYuxCqRMddomonvxfKoWYTA7kbfddce47KphApyQd+8ntST5SAuSP9q1JEtU2g9xgmlQD5iuOQ3OfuucNBZ50kADVMHG/1SS74Kk6myekihe6X0yANIgTvTBzKWI0iaJbUoZyzbdj3/iu1EmQAIj9M11y5JEY9uXMU9ONt8K7N5oURMzVAsWjbYBgxA5AzPrUfiuV0AwpMRyp1i6ttTAliY3iI50ukVWrV0+YsykbA7CCfnemAsHZQGMc9g394rVu22ViJBHIbZ7dKAq5lmIYqomemCP4rO76vhbhbkaVU8gTMgc5HzU95DbZXRgrCIyMHrHKmllJKoNLAGZESO35zqZ9TBoBkiSd5HrWojGuszkkyTuetD4jCZbB3rFtMzAHB2p7WVs2wXBJJpkibQIC7dgduo7VTcsBkN0cljO5OeVT2WLXFAaAcjt2q5kvshYvKYwDnaI9qHsAQrWxYuwLhgiP9hGDO1bbs3rdxraSLZOQQSIPL0PWj4YWbjLcLE3BvJiCMAVW48pJgjvn9an8awtQWQo+lZJAjpRaPCYJILKuROY6/XOttvbYeS4GifbqPXNEH129BEkTHtP8moswm4bjQtsFicyBjGMk4qvhrQ4jh2DqUcqWV8wDHMRtgmaWwuWlFoCHJkSMjrFPtcSbHDut0kuu0Rtgbjn/ADT4sk3aSbdmwg1voKASWBMMRMH3BqZrrspYsSSABMgNtgDuJiOtIuO926xUSqg6tWARt7zXI1i2xFpNQIkkbgbj/wAq4m9HxNwuukKxZCGKwZ5dqG3eWLgOnSFAPKJ7fPens0rqUQTmIycAkVNdNtL0uAZGQMdwCe5pEHwdoAiY8xxuIHTnnf8ASrdK8I2viIAgtAkkCDIg7Z7/ALVNd4n/AIyvcYaSSdK9e/715Vzib/FXGJYsXO35+b0zfV3Ho/8AJt3rp8BStkZOoyRn2+aaotrJtIwXOY5zGP4rz7PBs14WzlYgkGIO8GnA37enh0Tys2DEmIpn+JL9pt+7bW6f+xlcCCBvB37d52iiDJZsnQXdpIgTiB150g27YvRekNIIjM7zPStvXbr3gqrCA5A2ODme8Uw0vjQykF0CEjrJPeOVSrpAIDEHkI70/iFUqxLMCDIQ5I9T6GpuUgSP/as8S1QFReFLFiWbkNgDU5AG+RRDUV8uV2in2uEa4fNgHpyPKfWia7h7BdQzEaJ23maDiUW1dKKZG/feq0tnhrZUQxLDnuPw0trQHFW7lxQATkHHt1mp9XEaIzsQgJPKnWE2JB1bwwxvt6VRat/92oQuSAJ67fdUA6ATEnGOZiJFLaSJ3nhkLOJJkGOXvQLxBbVuQBiCMfzR8VeuspS4Vg7jl61CQFY6WB/TY0zS8vFbXAbbEYYjOYyd89e1TWzqdZMAHGKBSzMF39NzXocNYXwizKS5BI1bcoz71fD0++LHgalXWxG43Hef6qbwxxFlGGoMmCp2OOXtTAxLEp5NB04yDnIjqJFWIoUAgAgRJOJPOfg1PF9S2+GH+ItFJ/2Jkk+k4qluG1o4U6A40iOR670Q1uWdF8oBIbkcxA61gbxW0A6QVzAODTRPwv8A8+wl8G4+ojnsp51UwJsuwKkDO8Tk9fT9elKYNbUEKTzAg5n8/Wqk8G5wSm1aklvMAJIyY9Jpv1Z1Nbto6G2ECQ8wTBIzI/vt8Ho1OwIKdDGF9z60KKpvEXAQVkSZBzn9x90vi+JVVdVOl2IMA+380t0nIqucQvDu68QQ10iRck4EAR3HfG9RW1Nxpk6WJxETy/egv27l9fMBKCYLZj8FMV3HDKzAqSYAjaT+9KaEWh4up2lSwVVOxxnYnY1NxaPbfUCQJIlQRBJ7xVeotYBYiTkSRJwI+zSrrFmRLhKgvtuAZH7mkqUhuNuxCf5xp6kTEkd961lY+CLrMS5yCBJ26d6evCrbJa2BIElydpHT0H3WPYVdLuWcljpz/iJ5/ApqBY3LjL4tuQP9cR3n83pljhhw7M6YnBBzRrZ1KWbVqBmJG5G/xXMzKszjAOdsTzpbq/qmwtuyNUSoMmeZ5/xU/F8QmqWlQcmBiIPPl7VK16+yNcKsFkwBnvB6cvaKme4YK3NYgjDCAevpk0zvS34rtsSiypM4Dbj8zWXb3gqAFLMRJjkcQO+IxQ8JxARNEKSCCCTAE4j4k0dy/ZvsASUnAAgznB9M08pvPQ3bwJgQVYREZHeeZn8zUuAhVZjVIneM/wA0QdFlckA5PX8zQKSTABz2qyYlPs6g6eTbmME1Uj6WEwAWxAzsf1qdLfFLeRiNWgegAonm9fIdtv8AELzz/RphOGETxD6vMqgkdJxNZeL3HTwyF5SBB260viNVpVW2WIAzU9prxuAA6gpMHlsaSLuLLFjwny5aTOATt/dBxHEMz+UZBmDsMiP0ptp3bU2piQNgcA/vSXJthWeSjKZMZHvUPCXDXFQsGY5J5RnYUFxjZAUADVnGSMHn701uIldJ8oK+X19fSuXhWuKGbkMgbkxP6VdxPUwDSAYH0a9exbN+0jWywtjcb6sg7HpSk4BDBaWjHt+c6uBHDWEW28AEwk5P8yOlL0/556G0ieZMAnJ7mTJPrFDdKPFsJ5gSbjGcgDAHrz9BXNcuXbuoWxJUjSBGYJmqLFi63CsqiSYOcyCZwanjXqbh7d28gFpgrW20gbQN5HtV3EXrfD8KxtjM6Z6nrNQFXtuhQgalnVO4kj53+q7iHIsqgDEFicjERjtQ3Fdm4t9GvNcIdCp0yIME8u08u9KklnKOZY5jAIkdO/6Uvh4NsAAAMuT0xmkX7+m94aPA0nG5MGYH519pujuK4g2bptqoDAQDgnfIPatvrdeFVgGAid4xJFIAtR43ESxLADvil8RxjC6y2yAoO8belX8S2RdaDWz4TkkjIYmZAJ/quF62yOHdZG3r/IqZ+JLKNLEkjBOcgDJ9a89Rde6Cca2O5gTzFM0tW8SbjlLigMoGIMEHFHqewvmYPdYgkEmRMYj1FDcB4UKSxLNjAwBypdkm/e1XMsJ8wkEwOlPiLDf1W2KKYGeUE5GI5RTCxKllA8wHMZ+aUukuoUaFKkmdwevpFGWVRkSFxjftUxoNu+biKwkBzgnMxSGbirheJTlkzHefzNWcKqIdWI3gCIiMDtnmfSKZxVy0bYthdIYziCVzODy5CrPTOeobVq6ilLjzmQRkZiT8iu4hVAlk1EGS2OfL6ob917elUIJIORgCBFDw19Rci6rNLamKwNhMR0zsIq7U5pdz/wD0OjJb08pJ3z/FKYWyJXB2I6Y/un3WHjk2gArHAIA59OWaK5wrO8yNbZgDGO/WnEsSASYBAB5jtV3/AB/AS29sambGe4pfDWAt+LoOWgchEEzVt0XCpW1Ag7ncdKVZK87iOIZpQEgAmczJ5n6rLDNbIcCTyxsBOaUyPrIKwQRM43q7h5dGVQAq+UmP9en50q3MTtpdhLhvs2otG87E1ty0wKog0LOTtWniGW4yC2SC0AwBgb8qpuuNIUyX2EGcgxUXCoHDguBqQkc9h1qa/wD9lwpbeVcZBOB6fnKmXLfioAJtk8iTB++v61Rb4e3oWbYbTEZ36yR6fZqbh6lu8GbdnWRIVhAnPevQtqjqCywoAwdxiM/NANShRIuKu4bLAxO9OspqUOzAACYJz6VLdizjVU+GYkhN+/ajt20GtLZVyzzMzGMCfzehuoqAxpYEDIJkkjmfUxXcPxViwjqM3VOdJkEdfXp/dWfxfvVfB8Npd0cKSMk84k4j0xVNyFTUzAALMHYfn71Ba422z4DWySSTgzk4270q/cu8QgI5LkRvvB+xjtU7rWyQPFOt+8mgBbajVtEzz+TFAFUodRkHYT+1BxPEqstduKzkaQDsAII+/sUSvrRWGFEatzEbEUvjHqTjLt2y4BEIY0NsMbj3obAe7d8cgAxIn/bqaL/6K+ZWQlgeu2eccsGKBHaAGDEgT5RtjG0VpPCL7EuysxIH+MbEyKWltrzjImNqcLZQ+I1tgwPMbnPvyPOncObb3A4WABDdJ6xTxnNTpw1wXRmACZJ2EV6SW0UsCmoCCCeRG3pSuKOk2za1MA2Z5mRBPpBqgqC+pYMkEZwIMY/OVS3WpMLKA3DIx13k8q1UW3dLAHxDvB2HKsKnW0mFIwcwe/zRWUTV4hJOoAZOQOVT5iu1AOBpkkfA55rnCiGMycT0Ec/c0bMrgahCjnMHf+v5mucSvlIY8ickeo96aIhdYOni3AAGzABnPP0iI70F69ce6HB0ocKNgII3pt1mtWlY241LAnkZMe/3SVDtZZSdQJDCZzjp7b1rnrN5wDP4zgrAgAmMDv8AdMBVQrjUwMgyN/qlvCrpUeY4JHLrRoZZUAAEaSNozQddL3jK2+cjHaQKO2LqsFLzOYXJX+KfatvrDGZYf7bDGPXrS74Yq4s+Vw2SMSZ2+qbvFkEblm7d1F4CbZycbU3WEaXYERAM7npy7/1UfDWSLY8TBJJEjEb1SLSKgBEx/iDsDypcBP8A9ssFABG/U7c6y3a8ONEkMCGM47foaYYVNO5G2OfSgKtdslWOnVy5jpUimKukiVAUczuJpehSrWxIIOSMxneaGwkW4fdTgnJI71VxPDlLEoCwZYk5jHfrQnUdlrodtWlrS41MN+Uiq7TpcZQgkAwYgD7xSXGq0iqCAcMCZziI9aZbtG7AUaQgMCI5bD2pepv+Na6gujRJkgGViMZnbbrzp3B2f+TfDuNKJgjBDYP5+TXAcPw9tbRQlsc9+ojaNqz/AJJNvQAAFWlaky9dxGgTaszqXBWMwMzJ7zmvLvLesarriAWmCQC2d/zNX2NdniXvq0yoBU5BkZqT/wCmgceIW88kgDnkzI96k9S+KFfRaF+4qrqg6RJIn8+jU/EXbyALbDhTEFASfSkcE9y9aKnKLABMY3/mrL1wqFTdmOkEfRNXxN1CODJBe8xBJhVO5qq1avWli1JByQTJU8xP84zT7VlvDTxACyDBO57zXWUVrhuFiupTMnG4z+vxTQgX7FsAOJcdMzHKjS4TlFwYIMRudo/P4WIN7SqgOhkxtPX250xvDfiMGV0yGU422xzFKE8VxDrxFuRqVRBEERvP6inNdsWStnJdzMiIE9/mK6/ZFxEyNWdth0rzeJtG25ZG1KCATzBjIpJp49ZF/wCtZAcgxvtjn7GtW9oDW4Gth5QQRBAx6b7+9J4dRZS2qkklRLbfkYFMe2wckjAG++Z/ipzVYzkrBALZ8syJ33O1aAAgJnqREmBy+aU7F9hDW8lue5rLF1b/ABRAaTpiTsDzPzVPo3uArqYeUKZGwBA2g+31Qo124CoUKTMEbdpFOe1bwwGoKMAbE+nvRxcV0gjwzAPWfWpsJEfEv4mlCx0AeUmCR7YxNLVSiqjMAjGQc6jGw9ZFaygtJJIB3IyR+A/NbcEeZvKCQFEEme9a3iBYDQhAkkSBzHecfFYrB7kWkIAIJO8D1pYua7kNiBAPSrOEgWVBgM2TjJxypeHo3ueJadVbY6TqGCevzWLbe2pKJKgkwTJJxn596LQyMxidQgmQQOh6Y5Uqxde14iuxlgASTA7j5NRXLYFt9TkkqDscGOoita8yqXt+Yg7ZkCdvz2o3NsIrEgTsAQeXPPakv5dTAQSIid/yasxPDVurcCkmWMSTj2jauLK77EsSQDEBRmTSrdss4ccgIXBE9d42o+IuFzaZQSC/mMwczP3Sw6ubhrScPqBLMBmcA4nGO35Fdbt3r3ELbuS1h1h4bAE4Pfb9alPj37Y8zEHy84AkSa9m3bW3YS2GkadIO3Iwfip41JqQ/wDDsMsjU2ryEmRJiSewJ+Kc7KPDRlRSxAEGZAHM8s/zUdpLb8PdWRcIfCnBXlP3+YojbSyjC8oXIJZpBUenzUtWVlxdVw+Kioy5HOMTIHeayCLQ1ZMzO4A9Ky7dYMGuqWJEH0G1QjjgLZBkMW5bb5HpTtS42/x3gvC5IyZEe2KJf+xFaAA+QTEiT0+M471G1q5xk3lWADvOMc6rfVYsM+kwMSRsMCIq+M9cj27etADE7KDB/DWcNbUaiGJIMqWjygfv+1IbjLfhtEgydODOTyPSn8Ndmzq06RtJnMRTyBficSOJ1OSFMkDkYz7f+1YhLqxuKAJGANhPpPWpbj3HYXbQlMgTykjl6Udh7xVS7DQoJ8oktk7/ACPb3pZoetxVRlUAxOrmczv3qe+rqGQCZEyAQFg/fPamcO4uowQecDMDnyP1TEuOFAdpJAGBsJqeLhPDlhaChix1GTsVgg/zmjuWUuNLW1YhpmYAPSB35T8US3VS/wCdRofyswyQZ6fv1pV+5pZVtmYOojqcgiav4fDiFKyQTIgif8Yzv0xUt/jDcbQilQCQCDIPt1pNi3fuXSGdkRgTvkiYxVa2PBPiIkxuJ/yHM9qclT0DWg1wtcYqjbkSNXqJ6jl2p9hCjTpBJBEjnnP5vilXy2hWeVjdSNoOxNKHHAeVdTCIyII7/pUu0+rPFXxdMHSuJGw9filHiAFbTJB7jMn8/BUBv3LQ0hmExM4n8/mhS5CkGROSaf8AlLX/2Q==')");
                        self.setupDefaultTileLayer(opts.layerOpenstreetmap);
                    }
                }
                changeMap(opts.start, 1);

                controlA.addEventListener("mouseover", function () {
                    controlDiv.style.setProperty("height", "80px");
                });

                controlA.addEventListener("mouseout", function () {
                    controlDiv.style.setProperty("height", "30px");
                });

                controlA.addEventListener("click", function (event) {
                    let type = event.target.getAttribute("data-selected");
                    changeMap(type, 0);
                });

                return controlDiv;
            }
        });

        return new selectMap();
    }

}
