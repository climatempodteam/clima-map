(function () {

// Extends the map behavior
L.Map.include({
    restorePosition : function() {
        let storage = window.localStorage || {};
        try {
            const view = JSON.parse(storage['mapPos'] || '');
            this.setView(L.latLng(view.lat, view.lng), view.zoom, true);
            return true;
        } catch (err) {
            return false;
        }
    },
    savePosition : function() {
        try {
            let storage = window.localStorage || {};
            storage['mapPos'] = JSON.stringify({
                lat: this.getCenter().lat,
                lng: this.getCenter().lng,
                zoom: this.getZoom()
            });
            return true;
        } catch(err) {
            return false;
        }
    }
});

// Insert a simple button to save map current position
L.Control.SavePosition = L.Control.extend({
	options: {
		title: 'Mark this position as default in this device',
		savedMessage: 'Saved!',
		forceSeparateButton: false,
        customContainer : null,
        position: 'topleft'
    },

	onAdd: function (map) {
		var className = 'leaflet-control-saveposition pin-icon', container;

		if (this.options.customContainer !== null && ! this.options.forceSeparateButton) {
			container = this.options.customContainer;
		} else {
			container = L.DomUtil.create('div', 'leaflet-bar');
		}

		this._createButton(this.options.title, className, '', container, this.savePosition, this);
		return container;
	},

    savePosition: function () {
        var elm = document.querySelector('.saved-message');
        L.DomUtil.removeClass(elm, 'disnone');
        this._map.savePosition();

        /* to do: put this effect in css3 transition */
        setTimeout(function() {
            L.DomUtil.addClass(elm, 'disnone');
        }, 2000);

        return;
    },

	_createButton: function (title, className, content, container, fn, context) {
		this.link = L.DomUtil.create('a', className, container);
		this.link.href = '#';
		this.link.title = title;
		this.link.innerHTML = content;
        var savedMessage = L.DomUtil.create('div', 'saved-message disnone', this.link);
        savedMessage.innerHTML = this.options.savedMessage;

		L.DomEvent
			.addListener(this.link, 'click', L.DomEvent.stopPropagation)
			.addListener(this.link, 'click', L.DomEvent.preventDefault)
			.addListener(this.link, 'click', fn, context);

		return this.link;
	}
});

})();
