P: Por que estes plugins de terceiro estão aqui?
R: Estão forkados, porque foi necessário customizações 'low level' no código fonte e também para um build mais limpo (dispensando algumas features desnecessárias destes mesmos plugins).

P: Estes plugins recebem atualizações via npm ou bower?
R: Não. Eles estão forkados e customizados para os requisitos do projeto no qual este componente foi desenvolvido.

P: Todos os arquivos são incluídos no componente?
R: Nem todos, ver gulpfile.js

P: Onde estão os originais?
R: Em {node_modules}