/*
 * Leaflet.draw assumes that you have already included the Leaflet library.
 */

L.drawVersion = '0.3.0-dev';

L.drawLocal = {
	draw: {
		toolbar: {
			// #TODO: this should be reorganized where actions are nested in actions
			// ex: actions.undo  or actions.cancel
			actions: {
				title: 'Parar de desenhar',
				text: 'Cancelar'
			},
			finish: {
				title: 'Finalizar desenho',
				text: 'Finalizar'
			},
			undo: {
				title: 'Delete last point drawn',
				text: 'Delete last point'
			},
			buttons: {
				polyline: 'Calcular a distância entre dois pontos',
				circle: 'Obter informações de uma área',
			}
		},
		handlers: {
			circle: {
				tooltip: {
					start: 'Clique e arraste para desenhar um círculo'
				},
				radius: 'Raio'
			},
			polyline: {
				error: '<strong>Error:</strong> shape edges cannot cross!',
				tooltip: {
					start: 'Clique para adicionar o primeiro ponto',
					cont: 'Clique para finalizar a medição',
					end: 'Clique para finalizar a medição'
				}
			},
			simpleshape: {
				tooltip: {
					end: 'Pare de clicar para obter informações da área'
				}
			}
		}
	}
};
