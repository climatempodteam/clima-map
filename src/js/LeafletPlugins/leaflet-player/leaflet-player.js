(function () {

L.Control.PlayButton = L.Control.extend({
	options: {
		title: 'Play layers',
		forceSeparateButton: false,
        customContainer : null,
        position: 'topleft'
    },

	onAdd: function (map) {
		var className = 'leaflet-control-play-button cm-icon-play', container;

		if (this.options.customContainer !== null && ! this.options.forceSeparateButton) {
			container = this.options.customContainer;
		} else {
			container = L.DomUtil.create('div', 'leaflet-bar');
		}

		this._createButton(this.options.title, className, '', container, this.togglePlayer, this);
		return container;
	},

    togglePlayer: function (button) {
        if (L.DomUtil.hasClass(this.link, 'cm-icon-play')) {
            this._map.fire('player:play-clicked');
            L.DomUtil.removeClass(this.link, 'cm-icon-play');
            L.DomUtil.addClass(this.link, 'cm-icon-stop');

        } else {
            this._map.fire('player:stop-clicked');
            L.DomUtil.removeClass(this.link, 'cm-icon-stop');
            L.DomUtil.addClass(this.link, 'cm-icon-play');
        }
        return;
    },

	_createButton: function (title, className, content, container, fn, context) {
		this.link = L.DomUtil.create('a', className, container);
		this.link.href = '#';
		this.link.title = title;
		this.link.innerHTML = content;

		L.DomEvent
			.addListener(this.link, 'click', L.DomEvent.stopPropagation)
			.addListener(this.link, 'click', L.DomEvent.preventDefault)
			.addListener(this.link, 'click', fn, context);

		return this.link;
	}
});

})();
