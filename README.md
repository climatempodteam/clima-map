# Climamap

### Instruções pós checkout

Executar na raiz do projeto:

- npm update (vai atualizar o build stack [concatenação/compressão] dos js)
- bower update (vai atualizar as libs js dependentes para que este componente funcione)

#### Durante o desenvolvimento 

Executar na raiz do projeto:

- gulp

Isso fará com que o GULP rode em modo watch no diretório {src}, onde qualquer alteração em arquivos JS resultará em um build automático na pasta {dist} com os arquivos js minimificados e comprimidos.

### Para testar ###

Rode um local web server (p.ex.: php -S "localhost:8000") na raiz do projeto. Em seguida, entre em / (p.ex.: http://localhost:8000/).

### Coding Standards ###

- jsDoc, comentários em português identados por tab
- Variáveis/Funções em inglês
- Vale a pena a leitura do [Airbnb JavaScript Style Guide|https://github.com/airbnb/javascript]
- Habilitar jscs em seu IDE

### Utilização ###

- *Version 1.* *
   
  ``` 
      <script src="{{ asset('bower_components/climamap/dist/climamap.min.js') }}"></script>
  ``` 

 - *Version 2.* *    
    
     ``` 
          <script type="text/javascript" src="{{asset('bower_components/leaflet/dist/leaflet.js')}}"></script>
          <script src="{{ asset('bower_components/climamap/dist/climamap.min.js') }}"></script>
      ``` 
    
       
          <script>
                var opts = {
                        scale: true,
                        imagePath: "/img/icons/",
                        cache: 9999,
                        cacheBackward: 9999,
                        cacheForward: 9999,
                        tileLayerUrl: '//server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}.png',
                        controlSelectMap: {
                          layerSatellite: '//server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}.png',
                          layerOpenstreetmap: '//openmaps.climatempo.com.br/styles/klokantech-basic/{z}/{x}/{y}.png',
                          position: 'bottomright',
                          start: 'street'
                        }
                      };
      
                      monitor.cmap = new Climamap("map-canvas", L, opts);
               </script> 
               