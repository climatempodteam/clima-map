const { src, dest, watch, parallel } = require('gulp');
const { concat, rename, babel, uglify, jsdoc3 } = require('gulp-load-plugins')();
const jsBuildOrder = [
  //'src/js/LeafletPlugins/leaflet/leaflet10.js',
  'src/js/LeafletPlugins/leaflet-fullscreen/Control.FullScreen.js',
  'src/js/LeafletPlugins/leaflet-omnivore/leaflet-omnivore.js',
  'src/js/LeafletPlugins/leaflet-nontiledlayer/leaflet-nontiledlayer.js',
  'src/js/LeafletPlugins/leaflet-nontiledlayer/leaflet-nontiledlayer.wms.js',
  'src/js/LeafletPlugins/leaflet-timedimension/src/iso8601.min.js',
  'src/js/LeafletPlugins/leaflet-timedimension/src/leaflet.timedimension.js',
  'src/js/LeafletPlugins/leaflet-timedimension/src/leaflet.timedimension.util.js',
  'src/js/LeafletPlugins/leaflet-timedimension/src/leaflet.timedimension.layer.js',
  'src/js/LeafletPlugins/leaflet-timedimension/src/leaflet.timedimension.layer.wms.js',
  'src/js/LeafletPlugins/leaflet-timedimension/src/leaflet.timedimension.player.js',
  'src/js/LeafletPlugins/leaflet-timedimension/src/leaflet.timedimension.control.js',
  'src/js/LeafletPlugins/leaflet-draw/Leaflet.draw.js',
  'src/js/LeafletPlugins/leaflet-draw/draw/handler/Draw.Feature.js',
  'src/js/LeafletPlugins/leaflet-draw/draw/handler/Draw.Polyline.js',
  'src/js/LeafletPlugins/leaflet-draw/draw/handler/Draw.SimpleShape.js',
  'src/js/LeafletPlugins/leaflet-draw/draw/handler/Draw.Circle.js',
  'src/js/LeafletPlugins/leaflet-draw/ext/TouchEvents.js',
  'src/js/LeafletPlugins/leaflet-draw/ext/LatLngUtil.js',
  'src/js/LeafletPlugins/leaflet-draw/ext/GeometryUtil.js',
  'src/js/LeafletPlugins/leaflet-draw/ext/LineUtil.Intersect.js',
  'src/js/LeafletPlugins/leaflet-draw/ext/Polyline.Intersect.js',
  'src/js/LeafletPlugins/leaflet-draw/Control.Draw.js',
  'src/js/LeafletPlugins/leaflet-draw/Toolbar.js',
  'src/js/LeafletPlugins/leaflet-draw/Tooltip.js',
  'src/js/LeafletPlugins/leaflet-draw/draw/DrawToolbar.js',
  'src/js/LeafletPlugins/leaflet-saveposition/leaflet-saveposition.js',
  'src/js/LeafletPlugins/leaflet-player/leaflet-player.js',// ?
  'src/js/MapParts/AbstractMapPart.js',
  'src/js/MapParts/ImageLayer.js',
  'src/js/MapParts/TimeDimension.js',
  'src/js/MapParts/Kml.js',
  'src/js/MapParts/Wms.js',
  'src/js/MapParts/WmsSpecial.js',
  'src/js/MapParts/Marker.js',
  'src/js/MapParts/GeoJson.js',
  'src/js/MapParts/TileLayer.js',
  'src/js/Controls/AbstractControl.js',
  'src/js/Controls/Draw.js',
  'src/js/Controls/Fullscreen.js',
  'src/js/Controls/SavePosition.js',
  'src/js/Controls/Player.js', //?
  'src/js/Climamap.js' // Alterar?
];

function stylesheets() {
  return src([
      'src/js/LeafletPlugins/leaflet/leaflet.css',
      'src/css/leaflet.timedimension.control.css',
      'src/css/climamap.css'
    ])
    .pipe(concat('climamap.css'))
    .pipe(rename({ suffix: '.min' }))
    .pipe(dest('dist'));
};

// A idéia é distribuir o source da Leaflet junto com o do componente
function scripts() {
    return src(jsBuildOrder)
        .pipe(babel({
            presets: ['@babel/env']
        }))
        .pipe(concat('climamap.js'))
        .pipe(uglify().on('error', handleError))
        .pipe(rename({suffix: '.min'}))
        .pipe(dest('dist'))
        .on('error', handleError);
};

function doc(cb) {
    src(['README.md', 'src/**/*.js'], {read: false})
        .pipe(jsdoc3(cb))
        .pipe(dest('docs'));
};

function watcher() {
    watch('src/js/**/*.js', { ignoreInitial: false }, function update() {
      scripts();
      doc();
    });

    watch('src/css/**/*.css', { ignoreInitial: false }, stylesheets);
};

function handleError(err) {
  console.error(err.toString());
  this.emit('end');
}

exports.default = watcher;
exports.build = parallel(scripts, doc, stylesheets);
